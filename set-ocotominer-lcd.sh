#!/usr/bin/env bash
# Author: opselite@blockops.party
# Date: 5/21/22
# Purpose: Erase the octominer LCD screen and put something useful to it, create cron job to auto update thermals
#   - hostname
#   - ip address of main interface
#   - thermals  (although the data is static, recommend a cron job)
octofan=/opt/mmp/bin/octofan
# on hiveos it might be called fancontrol, on mmpos it is octofan

# where to put thermal scripts
cron_dir=/etc/cron.hourly

# Erase everything
sudo $octofan -o 0,0,4 -v ''

# Set the hostname on top
name=$(hostname)
sudo $octofan -o 0,0,3 -v ${name^^}

# ip address small at the top in small format
hostdata=($(hostname -I))
sudo $octofan -o 0,2,2 -v ${hostdata[0]}

# create script and copy to cron directory
cat << 'EOF' > /tmp/octo_lcd_screen.sh
#!/usr/bin/env bash
# Author: opselite@blockops.party
# Date: 5/21/22
octofan=/opt/mmp/bin/octofan
function setThermals() {
  thermals=($(sudo $octofan -h |grep Temp:))
  sudo $octofan -o 0,4,0 -v "${thermals[0]} ${thermals[1]}"
  sudo $octofan -o 0,5,0 -v "${thermals[2]} ${thermals[3]}"
  sudo $octofan -o 0,6,0 -v "${thermals[4]} ${thermals[5]}"
}
setThermals

EOF

sudo mkdir -p $cron_dir
sudo mv /tmp/octo_lcd_screen.sh $cron_dir/octo_lcd_screen.sh
sudo chmod +x $cron_dir/octo_lcd_screen.sh
sudo $cron_dir/octo_lcd_screen.sh
